import React from 'react'
import ComponenteCard from './ComponenteCard'
import yo from '../assets/yo.jpg'
import videojuegos from '../assets/videojuegos.jpg'
import anime from '../assets/anime.png'

const cards = [
    {
        id : 1,
        title : 'Foto',
        image: yo,
        texto: 'Mi nombre es Michael Dylan Ramos Benítez, tengo 22 años y estudio en el tecnologico de cuautla, actualmente estoy cursando dos materias, las cuales se llaman: Lenguajes de interfaz y Servicios Web en dispositivos móviles'
    },
    {
        id : 2,
        title : 'Juegos',
        image: videojuegos,
        texto: 'Mi pasatiempo favorito es jugar videojuegos en mi xbox, mi juego favorito por ahora se llama Elden Ring, me gusta pasar el tiempo jugando videojuegos para desestresarme'
    },
    {
        id : 3,
        title : 'Anime',
        image: anime,
        texto: 'Me gusta ver anime, y mi anime favorito es bleach, ya que de pequeño este fue uno de mis primeros animes, y por el cual también me empezo a gustar ver anime'
    }
]

function ComponenteCards() {
    console.log(cards)
  return (
    <div className='container d-flex  justify-content-center align-items-center h-100'>
        {/* d-flex  justify-content-center align-items-center h-100' */}
        <div className="row">
            {
                cards.map(card =>(
                   <div className="col-md-4" key={card.id}>
                        <ComponenteCard title={card.title} imageSrc={card.image} textoCard={card.texto}/>
                   </div> 
                ))
            }
        </div>
    </div>
  )
}

export default ComponenteCards