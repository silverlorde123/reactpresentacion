import React from 'react'
import './cards.css'


function ComponenteCard({title, imageSrc,textoCard}) {
  return (
    <div className="card text-center bg-dark animate__animated animate__fadeInUp">
        <div className="overflow">
            <img src={imageSrc} alt="" className='card-img-top'/>
        </div>
        <div className="card-body text-light">
            <h4 className="card-title">{title}</h4>
            <p className="card-text text-secondary">{textoCard}</p>
        </div>
    </div>
  )
}

export default ComponenteCard