import ComponenteCards from './components/ComponenteCards'
import './App.css'


function App() {

  return (
    <div className='App'>
      <ComponenteCards />
    </div>
  )
}

export default App
